//
//  main.m
//  Altimeter
//
//  Created by Mike Barriault on 2014-10-28.
//  Copyright (c) 2014 Mike Barriault. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

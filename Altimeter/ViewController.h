//
//  ViewController.h
//  Altimeter
//
//  Created by Mike Barriault on 2014-10-28.
//  Copyright (c) 2014 Mike Barriault. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>
#import <CoreLocation/CoreLocation.h>

@interface ViewController : UIViewController <CLLocationManagerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *latLabel;
@property (weak, nonatomic) IBOutlet UILabel *lonLabel;
@property (weak, nonatomic) IBOutlet UILabel *altLabel;
@property (strong) CMMotionManager *altManager;
@property (strong) CLLocationManager *locManager;
@property (strong) CMAltimeter *altimeter;
@property (strong) NSMutableArray* altitudeChanges;
@property (strong) NSMutableArray* locationChanges;
@property (strong) NSMutableArray* timestamps;
-(void) start;
-(void) stop;
@end


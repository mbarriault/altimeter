//
//  AppDelegate.h
//  Altimeter
//
//  Created by Mike Barriault on 2014-10-28.
//  Copyright (c) 2014 Mike Barriault. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (weak, nonatomic) ViewController* viewController;

@end

